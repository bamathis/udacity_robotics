# CMake generated Testfile for 
# Source directory: /home/workspace/Projects/Project3/src
# Build directory: /home/workspace/Projects/Project3/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(my_robot)
subdirs(teleop_twist_keyboard)
